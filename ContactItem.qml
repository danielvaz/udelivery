import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

import uDelivery.themes 1.0

Rectangle {
    id: root
    color: Themes.theme.background.normal
    property string contactName
    property string contactPhone
    property string contactAddress
    property var timePickerComponent: Qt.createComponent("TimePicker.qml")
    readonly property alias addButton: addProductButton
    width: parent.width
    height: 80

    Row {
        id: contactRowLayout
        anchors {
            left: parent.left
        }
        spacing: 10
        Image {
            id: contactIcon
            source: "images/business-contact-32.png"
            verticalAlignment: Image.AlignVCenter
        }
        Text {
            height: contactIcon.height
            text: contactName
            color: Themes.theme.font.color.textOnDarkBg
            font.pointSize: Themes.theme.font.size.big
            font.bold: true
            verticalAlignment: Text.AlignVCenter
        }
    }

    Column {
        id: columnLayout
        anchors {
            left: parent.left
            leftMargin: 50
            top: contactRowLayout.bottom
        }
        Text {

            text: contactPhone
            font.pointSize: Themes.theme.font.size.normal
            color: Themes.theme.font.color.textOnDarkBg
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: contactAddress
            font.pointSize: Themes.theme.font.size.normal
            color: Themes.theme.font.color.textOnDarkBg
            verticalAlignment: Text.AlignVCenter
        }
    }

    PButton {
        id: addProductButton
        anchors {
            bottom: parent.bottom
            bottomMargin: -1
            verticalCenter: parent.verticalCenter
            left: columnLayout.right
            leftMargin: 100
        }
        height: 81
        borderWidth: 1
        borderColor: Themes.theme.background.light
        colorPressed: Themes.theme.background.dark
        iconSource: "images/Buy-64.png"
    }

    Row {
        height: 60
        anchors {
            left: addProductButton.right
        }

        Text {
            height: 30
            verticalAlignment: Text.AlignVCenter
            color: Themes.theme.font.color.textOnDarkBg
            font.pointSize: Themes.theme.font.size.normal
            text: "Horário de entrega:"
        }

        CustomSpinBox {
            minimumValue: 0
            maximumValue: 23
            Component.onCompleted: {
                var now = new Date()
                value = now.getHours()
            }
        }

        CustomSpinBox {
            minimumValue: 0
            maximumValue: 59
            Component.onCompleted: {
                var now = new Date()
                value = now.getMinutes()
            }
        }

        Button {
            onClicked: {
                timePickerComponent.createObject(root)
            }
        }

    }
}

