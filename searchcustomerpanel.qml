import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import uERP.DB 1.0

Item {
    id: root
    property var newOrderPanel: Qt.createComponent("neworderpanel.qml")

    Rectangle {
        anchors.fill: parent
        color: "#ffffff"
        TopBar {
            id: topBar
            title: "Buscar Cliente"
            okButton.onClicked: {
                stackViewPanel.pop()
            }
            cancelButton.onClicked: {
                stackViewPanel.pop()
            }
        }
        DeliveryTextInput {
            id: searchBoxInput
            placeholderText: "Digite ao menos três números"
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: 15
                rightMargin: 15
                top: topBar.bottom
                topMargin: 15
            }
            onTextChanged: {
                if ( text.length >= 3 ) {
                    DatabaseManager.customerManager.selectByPhone(text)
                } else {
                    DatabaseManager.customerManager.customers = []
                }
            }
        }

        ListView {
            id: contactList
            spacing: 10
            anchors {
                top: searchBoxInput.bottom
                bottom: parent.bottom
                left: parent.left
                margins: 30
            }
            width: parent.width - anchors.margins
            model: DatabaseManager.customerManager.customers
            delegate: ContactDelegate {
                newOrderButton.visible: true
                newOrderButton.onClicked: {
                    var props = { 'width': root.width, 'height': root.height,
                        "customer": model.modelData
                    }
                    stackViewPanel.push(newOrderPanel.createObject(root, props))
                }
            }
        }
    }
}
