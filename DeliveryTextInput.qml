import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1

import uDelivery.themes 1.0

TextField {
    id: root
    property int fontPixelSize: 26

    style: TextFieldStyle {
        textColor: Themes.theme.font.color.textOnLightBg
        placeholderTextColor: Themes.theme.font.color.disabled
        font.pixelSize: root.fontPixelSize

        background: Item {
            implicitHeight: 50
            implicitWidth: 350

            BorderImage {
                source: "images/textinput.png"
                border.left: 8
                border.right: 8
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }
        }
    }

    function setValue(value) {
        root.text = value
    }
}
