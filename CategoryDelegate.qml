import QtQuick 2.5
import uERP.DB 1.0
import uDelivery.themes 1.0

Rectangle {
    id: root
    property ListModel productByCategory: ListModel { }
    color: Themes.theme.background.light

    Component.onCompleted: {
        var categories = DatabaseManager.productCategoryManager.categories
        for(var i=0; i<categories.length; ++i) {
            var category = categories[i]
            console.log("categoria", i,  category.name)
            DatabaseManager.productManager.selectByCategory(category.code)
            var lst = DatabaseManager.productManager.products
            for(var j=0; j<lst.length; ++j) {
                var product = lst[j]
                product.categoryLabel = category.name
                console.log(j, product)
                productByCategory.append(product)
            }
        }
    }

    ListView {
        anchors.fill: parent
        model: productByCategory
        section.property: "categoryLabel"
        section.criteria: ViewSection.FullString
        section.delegate: Rectangle {
            width: parent.width
            height: childrenRect.height + 4
            color: Themes.theme.background.normal
            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                text: section
            }
        }
        delegate: ProductDelegate {
            customizeButton.iconSource: "images/Plus-64.png"
            customizeButton.onClicked: {
                products.append(model)
            }
        }
    }
}
