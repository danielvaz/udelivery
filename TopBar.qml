import QtQuick 2.3

import uDelivery.themes 1.0

Item {
    id: root
    property string title
    readonly property alias okButton: okButton
    readonly property alias cancelButton: cancelButton
    property alias bgColor: background.color
    width: parent.width
    height: 64

    Rectangle {
        id: background
        anchors.fill: parent
        color: Themes.theme.background.dark
        anchors.top: parent.top

        PButton {
            id: cancelButton
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            iconSource: "images/cancel-64.png"
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "#ffffff"
            font.pixelSize: 26
            text: title
        }

        PButton {
            id: okButton
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            iconSource: "images/ok-64.png"
        }
    }
}
