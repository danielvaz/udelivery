import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

import uDelivery.themes 1.0
import uERP.DB 1.0

Item {
    id: root

    ListModel {
        id: products
    }

    signal productListReady(ListModel list)

    Rectangle {
        anchors.fill: parent
        color: Themes.theme.background.light
    }

    TopBar {
        id: topBar
        title: "Buscar produto"
        okButton.onClicked: {
            productListReady(products)
            stackViewPanel.pop()
        }
        cancelButton.onClicked: {
            stackViewPanel.pop()
        }
    }

    DeliveryTextInput {
        id: searchBoxInput
        width: parent.width
        placeholderText: "Pesquisa por categoria ou nome"
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: topBar.bottom
            topMargin: 15
        }
        onTextChanged: {
            if ( text.length > 4 ) {
                // FIXME: chamar função c++ para retornar lista de produtos com certo limite
            }
        }
    }

    CategoryDelegate {
        anchors {
            top: searchBoxInput.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: 30
        }
    }
}
