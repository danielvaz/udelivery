import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

import uDelivery.themes 1.0

Rectangle {
    width: parent.width
    height: 40
    color: Themes.theme.background.light
    property alias customizeButton: customButton
    Text {
        id: pName
        anchors.left: parent.left
        width: 350
        elide: Text.ElideRight
        height: parent.height
        font.pointSize: Themes.theme.font.size.big
        verticalAlignment: Text.AlignVCenter
        text: model.name
        color: Themes.theme.font.color.textOnLightBg
    }
    Text {
        id: pPrice
        anchors {
            left: pName.right
        }
        width: 100
        height: parent.height
        font.pointSize: Themes.theme.font.size.big
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: model.price.toFixed(2)
        color: Themes.theme.font.color.textOnLightBg
    }
    SpinBox {
        id: pQuantity
        anchors {
            left: pPrice.right
        }
        height: parent.height
        width: 100
        minimumValue: 1
        value: 1
        font.pointSize: Themes.theme.font.size.big

        style: SpinBoxStyle {
            selectionColor: "white"
            selectedTextColor: "black"
            incrementControl: Image {
                source: "images/Triangular Arrow Up-24.png"
            }
            decrementControl: Image {
                source: "images/Triangular Arrow Down-24.png"
            }
            background: Rectangle {
                anchors.fill: parent
                color: Themes.theme.background.light
                border.width: 0
                anchors.margins: 1
            }

        }
    }
    Text {
        id: totalPrice
        anchors {
            left: pQuantity.right
        }
        width: 200
        height: parent.height
        font.pointSize: Themes.theme.font.size.big
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: (pQuantity.value * model.price).toFixed(2)
    }
    PButton {
        id: customButton
        anchors.left: totalPrice.right
        anchors.verticalCenter: totalPrice.verticalCenter
        height: parent.height - 10
        width: height
        borderRadius: 50
        borderWidth: 2
    }
}
