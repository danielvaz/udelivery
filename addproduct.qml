import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import uERP.DB 1.0

Item {
    property ListModel categories: ListModel { }
    property Product product: Product {
        name: nameInput.text
        code: parseInt(codeInput.text)
        category: categories.get(categoryInput.currentIndex).code
        cost: parseFloat(costInput.text)
        price: parseFloat(priceInput.text)
        description: descriptionInput.text
    }

    Component.onCompleted: {
        var lst = DatabaseManager.productCategoryManager.categories
        for (var i=0; i< lst.length; ++i) {
            var obj = lst[i]
            categories.append({"text" : obj.name, "code": obj.code})
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#ffffff"
        TopBar {
            id: topBar
            title: "Novo Produto"
            okButton.onClicked: {
                DatabaseManager.productManager.addProduct(product)
                stackViewPanel.pop()
            }
            cancelButton.onClicked: {
                stackViewPanel.pop()
            }
        }
    }

    Grid {
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: topBar.height/2
        }
        columns: 2
        spacing: 10

        Text {
            text: "Categoria:"
            font.pixelSize: 26
            font.capitalization: Font.SmallCaps
            verticalAlignment: Text.AlignBottom
            height: categoryInput.height
        }
        ComboBox {
            id: categoryInput
            width: 350
            model: categories
            onCurrentIndexChanged: {
                console.log("onCurrentIndexChanged", currentIndex)
            }

            style: ComboBoxStyle {
                id: comboBox

                background: Rectangle {
                    id: rectCategory
                    radius: 5
                    border.width: 2
                    border.color: "#e74c3c"
                    color: "transparent"
                    Image {
                        width: control.height
                        height: control.height
                        anchors{
                            right: parent.right
                            top: parent.top
                            rightMargin: 10
                        }
                        source: "images/expand-arrow-50.png"
                    }
                }
                label: Text {
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 26
                    color: "black"
                    text: control.currentText
                }
            }
        }

        Text {
            text: "Código:"
            font.pixelSize: 26
            font.capitalization: Font.SmallCaps
            verticalAlignment: Text.AlignBottom
            height: codeInput.height
        }
        DeliveryTextInput {
            id: codeInput
            verticalAlignment: TextInput.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            validator: IntValidator { }
        }

        Text {
            text: "Nome:"
            font.pixelSize: 26
            font.capitalization: Font.SmallCaps
            verticalAlignment: Text.AlignBottom
            height: nameInput.height
        }
        DeliveryTextInput {
            id: nameInput
            verticalAlignment: TextInput.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            text: "Custo:"
            font.pixelSize: 26
            font.capitalization: Font.SmallCaps
            verticalAlignment: Text.AlignBottom
            height: priceInput.height
        }
        DeliveryTextInput {
            id: costInput
            verticalAlignment: TextInput.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            validator: DoubleValidator { bottom: 0.0; decimals: 2 }
        }

        Text {
            text: "Preço:"
            font.pixelSize: 26
            font.capitalization: Font.SmallCaps
            verticalAlignment: Text.AlignBottom
            height: priceInput.height
        }
        DeliveryTextInput {
            id: priceInput
            verticalAlignment: TextInput.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            validator: DoubleValidator { bottom: 0.0; decimals: 2 }
        }

        Text {
            text: "Descrição:"
            font.pixelSize: 26
            font.capitalization: Font.SmallCaps
            verticalAlignment: Text.AlignVCenter
            height: descriptionInput.height
        }
        DeliveryTextArea {
            id: descriptionInput
        }
    }
}
