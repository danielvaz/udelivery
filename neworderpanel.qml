import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

import uDelivery.themes 1.0

Item {
    id: root
    property var customer: ({})
    property var chooseProductPanel: Qt.createComponent("chooseproductpanel.qml")
    property ListModel products: ListModel {}

    Rectangle {
        anchors.fill: parent
        color: Themes.theme.background.light
    }

    TopBar {
        id: topBar
        title: "Novo pedido"
        okButton.onClicked: {
            stackViewPanel.pop()
        }
        cancelButton.onClicked: {
            stackViewPanel.pop()
        }
    }

    ContactItem {
        id: contactInfoBar
        anchors {
            top: topBar.bottom
            topMargin: 1
            horizontalCenter: root.horizontalCenter
        }
        contactName: customer.name
        contactPhone: customer.phone
        contactAddress: customer.addressStreet
        addButton.onClicked: {
            var obj = chooseProductPanel.createObject(root)
            stackViewPanel.push(obj)
            obj.productListReady.connect(setProductList)
        }
    }

    Rectangle {
        id: aboveSeparator
        height: 5
        width: parent.width
        anchors.top: contactInfoBar.bottom
        gradient: Gradient {
            GradientStop { position: 0.0; color: Themes.theme.background.dark }
            GradientStop { position: 0.5; color: Themes.theme.background.light }
            GradientStop { position: 1.0; color: Themes.theme.background.normal }
        }
    }

    Rectangle {
        id: tableInfo
        anchors {
            top: aboveSeparator.bottom
            left: parent.left
            right: parent.right
        }
        height: 40
        color: Themes.theme.background.normal
        Text {
            id: pName
            anchors.left: parent.left
            width: 350
            elide: Text.ElideRight
            height: parent.height
            font.pointSize: Themes.theme.font.size.big
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "#ffffff"
            text: qsTr("Produto")
        }
        Rectangle {
            id: rectName
            anchors.right: pName.right
            width: 1
            height: parent.height
        }

        Text {
            id: pPrice
            anchors {
                left: rectName.right
            }
            width: 100
            height: parent.height
            font.pointSize: Themes.theme.font.size.big
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "#ffffff"
            text: qsTr("Preço")
        }
        Rectangle {
            id: rectPrice
            anchors.right: pPrice.right
            width: 1
            height: parent.height
        }
        Text {
            id: pQuantity
            anchors {
                left: rectPrice.right
            }
            width: 100
            height: parent.height
            font.pointSize: Themes.theme.font.size.big
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "#ffffff"
            text: qsTr("Qtde")
        }
        Rectangle {
            id: rectQuantity
            anchors.right: pQuantity.right
            width: 1
            height: parent.height
        }
        Text {
            id: totalPrice
            anchors {
                left: rectQuantity.right
            }
            width: 200
            height: parent.height
            font.pointSize: Themes.theme.font.size.big
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "#ffffff"
            text: qsTr("Preço total")
        }
    }

    Rectangle {
        id: bellowSeparator
        height: 1
        width: parent.width
        anchors.top: tableInfo.bottom
    }

    ListView {
        anchors {
            top: bellowSeparator.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        model: products
        delegate: ProductDelegate {
            customizeButton.iconSource: "images/Delete-24.png"
            customizeButton.onClicked: {
                products.remove(model.index)
            }
        }
    }

    function setProductList( pList ) {
        products = pList
    }

}
