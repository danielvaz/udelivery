pragma Singleton
import QtQuick 2.3

QtObject {
    property QtObject theme
    property QtObject simpleRed: QtObject {
        property string name: "Simples"
        property QtObject background : QtObject {
            property color dark: "#D32F2F"
            property color normal: "#F44336"
            property color light: "#FFAAAA"
        }
        property QtObject font : QtObject {
            property QtObject color : QtObject {
                property color textOnLightBg: "#dd000000"
                property color textOnDarkBg: "#ffffff"
                property color secondaryTextOnLightBg: "#cccccc"
                property color disabled: "#60000000"
            }
            property QtObject size: QtObject {
                property int small: 12
                property int normal: 14
                property int big: 16
                property int huge: 18
            }
        }
    }

    property QtObject simpleBlue: QtObject {
        property string name: "Simples"
        property QtObject background : QtObject {
            property color darker: "#392bc0"
            property color dark: "#4c3ce7"
            property color light: "#5c4ce7"
            property color lighter:"#6c5ce7"
        }
    }
    Component.onCompleted: {
        theme = simpleRed
    }
}
