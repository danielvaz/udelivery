import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Button {
    id: pButton

    property color colorPressed: "#11FFFFFF"
    property color colorReleased: "transparent"
    property color colorDisabled: "transparent"
    property color textColorDisabled: "#787878"
    property color textColorEnabled: "#FFFFFF"
    readonly property color textColor: textColorEnabled

    property int fontCapitalization: Font.MixedCase
    property bool bold: false
    property int fontSize: 12

    property int borderWidth: 0
    property color borderColor: "#CCFFFFFF"
    property color borderColorDisabled: "#787878"
    property int borderRadius: 0

    width: 80
    height: 80

    function setFontCapitalization(type) {
        fontCapitalization = type
    }

    onFontCapitalizationChanged: {
        if (fontCapitalization == Font.AllUppercase)
            text = text.toUpperCase()
        else
            text = text.toLowerCase()
    }

    onEnabledChanged: {
        if (enabled) {
            textColor = textColorEnabled
        } else {
            textColor = textColorDisabled
        }
    }

    style: ButtonStyle {
        background: Rectangle {
            color: {
                if (enabled) {
                    if (!checked) {
                        pButton.pressed ? colorPressed : colorReleased
                    }
                    else if (checked) {
                        pButton.checked ? colorPressed : colorReleased
                    }
                }
                else {
                    pButton.enabled ? colorReleased : colorDisabled
                }
            }

            border.width: pButton.borderWidth
            radius: pButton.borderRadius

            border.color: {
                if (enabled) {
                    pButton.borderColor
                }
                else {
                    pButton.borderColorDisabled
                }
            }
        }

        label: Item{
            Row {
                id: row
                anchors.centerIn: parent
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    color: textColor
                    text: control.text
                    font.capitalization: fontCapitalization
                    font.bold: bold
                    font.pointSize: fontSize
                    verticalAlignment: Text.AlignVCenter
                }
                Image {
                    source: pButton.iconSource
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Image.AlignVCenter
                }
            }
        }

    }
}
