#-*- coding: utf-8 -*-

import os

class Parser(object):
    def __init__(self, className, propertyName, dataObject, dataObjectHeader):
        self.headerTemplate = open("ManagerTemplate.header", 'r')
        self.sourceTemplate = open("ManagerTemplate.source", 'r')
        self.className = className
        self.propertyName = propertyName
        self.dataObject = dataObject
        self.dataObjectHeader = dataObjectHeader
    
    def generateHeader(self):
        headerName = self.className.lower() + ".h"
        if os.path.isfile(headerName):
            raise Exception("File %s already exists" % headerName)
        managerHpp = open(headerName, 'w')
        for line in self.headerTemplate:
            managerHpp.write(self.replaceHeaderMacros(line))
        managerHpp.close()
        
    def generateSource(self):
        sourceName = self.className.lower() + ".cpp"
        if os.path.isfile(sourceName):
            raise Exception("File %s already exists" % sourceName)
        managerCpp = open(sourceName, 'w')
        for line in self.sourceTemplate:
            managerCpp.write(self.replaceSourceMacros(line))
        managerCpp.close()
    
    def replaceHeaderMacros(self, line):
        aux = line.replace('$ClassName', self.className)
        aux = aux.replace('$CLASSNAME', self.className.upper())
        aux = aux.replace('$PropertyName', self.propertyName)
        aux = aux.replace('$DataAccessObject', self.dataObject)
        aux = aux.replace('$DataAcessObjectHeader', self.dataObjectHeader)
        return aux
    
    def replaceSourceMacros(self, line):
        aux = line.replace('$ClassName', self.className)
        aux = aux.replace('$classname', self.className.lower())
        aux = aux.replace('$PropertyName', self.propertyName)
        aux = aux.replace('$DataAccessObject', self.dataObject)
        return aux

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 5:
        print("Usage %s ManagerClassName PropertyName DataObjectName DataObjectHeader")
    else:
        a = Parser(*sys.argv[1:])
        a.generateHeader()
        a.generateSource()
