import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

TextArea {
    id: root
    property int fontPixelSize: 26
    implicitWidth: 350

    style: TextAreaStyle {
        textColor: "black"
        font.pixelSize: root.fontPixelSize
    }
}
