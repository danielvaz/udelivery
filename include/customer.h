#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QtQml>

#include <QObject>

class Customer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY phoneChanged)
    Q_PROPERTY(QString addressStreet READ addressStreet WRITE setAddressStreet NOTIFY addressStreetChanged)
    Q_PROPERTY(int addressNumber READ addressNumber WRITE setAddressNumber NOTIFY addressNumberChanged)
    Q_PROPERTY(QString addressComplement READ addressComplement WRITE setAddressComplement NOTIFY addressComplementChanged)
    Q_PROPERTY(QString neighborhood READ neighborhood WRITE setNeighborhood NOTIFY neighborhoodChanged)
    Q_PROPERTY(QString zipCode READ zipCode WRITE setZipCode NOTIFY zipCodeChanged)
public:
    explicit Customer(QObject *parent = 0) :
        QObject(parent) {
    }

    QString name() const {
        return m_name;
    }

    QString phone() const {
        return m_phone;
    }

    QString addressStreet() const {
        return m_addressStreet;
    }

    int addressNumber() const {
        return m_addressNumber;
    }

    QString zipCode() const {
        return m_zipCode;
    }

    QString addressComplement() const {
        return m_addressComplement;
    }

    QString neighborhood() const {
        return m_neighborhood;
    }

    static void declareQml() {
        qmlRegisterType<Customer>("uERP.DB", 1, 0, "Customer");
    }

signals:

    void nameChanged(QString arg);

    void phoneChanged(QString arg);

    void addressStreetChanged(QString arg);

    void addressNumberChanged(int arg);

    void zipCodeChanged(QString arg);

    void addressComplementChanged(QString arg);

    void neighborhoodChanged(QString neighborhood);

public slots:

    void setName(QString arg) {
        if (m_name != arg) {
            m_name = arg;
            emit nameChanged(arg);
        }
    }

    void setPhone(QString arg) {
        if (m_phone != arg) {
            m_phone = arg;
            emit phoneChanged(arg);
        }
    }

    void setAddressStreet(QString arg) {
        if (m_addressStreet != arg) {
            m_addressStreet = arg;
            emit addressStreetChanged(arg);
        }
    }

    void setAddressNumber(int arg) {
        if (m_addressNumber != arg) {
            m_addressNumber = arg;
            emit addressNumberChanged(arg);
        }
    }

    void setZipCode(QString arg) {
        if (m_zipCode != arg) {
            m_zipCode = arg;
            emit zipCodeChanged(arg);
        }
    }

    void setAddressComplement(QString arg) {
        if (m_addressComplement != arg) {
            m_addressComplement = arg;
            emit addressComplementChanged(arg);
        }
    }

    void setNeighborhood(QString neighborhood)
    {
        if (m_neighborhood == neighborhood)
            return;

        m_neighborhood = neighborhood;
        emit neighborhoodChanged(neighborhood);
    }

private:
    QString m_name;
    QString m_phone;
    int m_addressNumber;
    QString m_addressStreet;
    QString m_zipCode;
    QString m_addressComplement;
    QString m_neighborhood;
};

#endif // CUSTOMER_H
