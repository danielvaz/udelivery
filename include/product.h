#ifndef PRODUCT
#define PRODUCT

#include <QtQml>
#include <QObject>

class Product : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString code READ code WRITE setCode NOTIFY codeChanged)
    Q_PROPERTY(double price READ price WRITE setPrice NOTIFY priceChanged)
    Q_PROPERTY(double cost READ cost WRITE setCost NOTIFY costChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(int category READ category WRITE setCategory NOTIFY categoryChanged)

public:
    Product(QObject *parent = Q_NULLPTR)
        : QObject(parent) { }

    QString name() const
    {
        return m_name;
    }

    QString code() const
    {
        return m_code;
    }

    double price() const
    {
        return m_price;
    }

    double cost() const
    {
        return m_cost;
    }

    QString description() const
    {
        return m_description;
    }

    int category() const
    {
        return m_category;
    }

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    void setCode(QString code)
    {
        if (m_code == code)
            return;

        m_code = code;
        emit codeChanged(code);
    }

    void setPrice(double price)
    {
        if (m_price == price)
            return;

        m_price = price;
        emit priceChanged(price);
    }

    void setCost(double cost)
    {
        if (m_cost == cost)
            return;

        m_cost = cost;
        emit costChanged(cost);
    }

    void setDescription(QString description)
    {
        if (m_description == description)
            return;

        m_description = description;
        emit descriptionChanged(description);
    }

    void setCategory(int category)
    {
        if (m_category == category)
            return;

        m_category = category;
        emit categoryChanged(category);
    }

    static void declareQml() {
        qmlRegisterType<Product>("uERP.DB", 1, 0, "Product");
    }

signals:
    void nameChanged(QString name);

    void codeChanged(QString code);

    void priceChanged(double price);

    void costChanged(double cost);

    void descriptionChanged(QString description);

    void categoryChanged(int category);

private:
    QString m_name;
    QString m_code;
    double m_price;
    double m_cost;
    QString m_description;
    int m_category;
};

#endif // PRODUCT
