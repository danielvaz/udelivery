#ifndef SQLITEQTTYPEMATCHER_H
#define SQLITEQTTYPEMATCHER_H

#include <QDebug>
#include <QHash>
#include <QVariant>

class SqliteQtTypeMatcher
{
public:
    SqliteQtTypeMatcher();
    QString getSqliteType(QVariant::Type qtType) const;
private:
    QHash <QVariant::Type, QString> m_typeMatching;
};

#endif // SQLITEQTTYPEMATCHER_H
