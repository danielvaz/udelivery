#ifndef PRODUCTMANAGER_H
#define PRODUCTMANAGER_H

#include <memory>

#include <QObject>
#include <QQmlListProperty>

#include "abstracttable.h"
#include "product.h"

class ProductManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Product> products READ products NOTIFY productsChanged)
public:
    explicit ProductManager(SqliteDB& db, QObject *parent = 0);
    virtual ~ProductManager();

    Q_INVOKABLE bool addProduct(Product* c);

    static int declareQml() {
        return qmlRegisterUncreatableType<ProductManager>("uerp.DB", 1, 0,
                                                          "ProductManager",
                                                          "ProductManager is not creatable");
    }

    QQmlListProperty<Product> products()
    {
        return QQmlListProperty<Product>(this, 0, &ProductManager::append_dao,
                                         &ProductManager::count_dao,
                                         &ProductManager::at_dao,
                                         &ProductManager::clear_dao);
    }

    Q_INVOKABLE void select(const QString& whereClause = QString() );

    Q_INVOKABLE void selectByCategory(const int category);

signals:

    void productsChanged();

public slots:
protected:
    void iterateOverSelect();
private:

    static void append_dao(QQmlListProperty<Product> *list, Product *dao) {
        ProductManager *manager = qobject_cast<ProductManager *>(list->object);
        if ( manager ) {
            manager->m_products.append(dao);
            manager->productsChanged();
        }
    }

    static Product* at_dao(QQmlListProperty<Product> *list, int index) {
        ProductManager *manager = qobject_cast<ProductManager *>(list->object);
        if ( manager ) {
            return manager->m_products.at( index );
        }
        return 0;
    }

    static int count_dao(QQmlListProperty<Product> *list) {
        ProductManager *manager = qobject_cast<ProductManager *>(list->object);
        if ( manager ) {
            return manager->m_products.size();
        }
        return 0;
    }

    static void clear_dao(QQmlListProperty<Product> *list) {
        ProductManager *manager = qobject_cast<ProductManager *>(list->object);
        if ( manager ) {
            manager->m_products.clear();
            manager->productsChanged();
        }
    }

    SqliteDB &m_db;
    std::unique_ptr< AbstractTable<Product> > m_table;
    QList<Product* > m_products;
};

#endif // PRODUCTMANAGER_H
