#ifndef SQLITEDB_H
#define SQLITEDB_H

#include <QDebug>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>

class SqliteDB : public QSqlDatabase
{
public:
    SqliteDB(QSqlDatabase db);
    SqliteDB(QSqlDatabase db, int maxOperationsPerTransaciton);
    virtual ~SqliteDB();

    /**
     * @brief openDB
     * Open the database connection and creates a new transaction
     * @return
     */
    bool openDB();

    QSqlQuery doExec(const QString& str = QString());

    int newOperation();

    bool tableExists(const QString& tblName);

protected:
    /**
     * @brief doTransaction
     * Commits the current transaction and open a new one.
     * @return
     */
    bool doTransaction();

    /**
     * @brief doCommit
     * Commits the current transaction
     */
    bool doCommit();
    int m_maxOperationsPerTransaction;
    int m_currentOperation;
};

#endif // SQLITEDB_H
