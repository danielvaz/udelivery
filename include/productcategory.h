#ifndef PRODUCTCATEGORY
#define PRODUCTCATEGORY

#include <QtQml>
#include <QObject>

class ProductCategory : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int code READ code WRITE setCode NOTIFY codeChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

public:
    ProductCategory(QObject *parent = Q_NULLPTR)
        : QObject(parent) { }

    int code() const
    {
        return m_code;
    }

    QString name() const
    {
        return m_name;
    }

    void setCode(int code)
    {
        if (m_code == code)
            return;

        m_code = code;
        emit codeChanged(code);
    }

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    static void declareQml() {
        qmlRegisterType<ProductCategory>("uERP.DB", 1, 0, "ProductCategory");
    }

signals:
    void codeChanged(int code);
    void nameChanged(QString name);

private:
    int m_code;
    QString m_name;
};

#endif // PRODUCTCATEGORY

