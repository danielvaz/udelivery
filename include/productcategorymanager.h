#ifndef PRODUCTCATEGORYMANAGER_H
#define PRODUCTCATEGORYMANAGER_H

#include <memory>

#include <QObject>
#include <QQmlListProperty>

#include "abstracttable.h"
#include "productcategory.h"

class ProductCategoryManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<ProductCategory> categories READ categories NOTIFY categoriesChanged)
public:
    explicit ProductCategoryManager(SqliteDB& db, QObject *parent = 0);
    virtual ~ProductCategoryManager();

    Q_INVOKABLE bool addProductCategory(ProductCategory* c);

    static int declareQml() {
        return qmlRegisterUncreatableType<ProductCategoryManager>("uerp.DB", 1, 0,
                                                                  "ProductCategoryManager",
                                                                  "ProductCategoryManager is not creatable");
    }

    QQmlListProperty<ProductCategory> categories()
    {
        return QQmlListProperty<ProductCategory>(this, 0, &ProductCategoryManager::append_dao,
                                                 &ProductCategoryManager::count_dao,
                                                 &ProductCategoryManager::at_dao,
                                                 &ProductCategoryManager::clear_dao);
    }

    Q_INVOKABLE void select(const QString& whereClause = QString() );

signals:

    void categoriesChanged();

public slots:
protected:
    void iterateOverSelect();
private:

    static void append_dao(QQmlListProperty<ProductCategory> *list, ProductCategory *dao) {
        ProductCategoryManager *manager = qobject_cast<ProductCategoryManager *>(list->object);
        if ( manager ) {
            manager->m_categories.append(dao);
            manager->categoriesChanged();
        }
    }

    static ProductCategory* at_dao(QQmlListProperty<ProductCategory> *list, int index) {
        ProductCategoryManager *manager = qobject_cast<ProductCategoryManager *>(list->object);
        if ( manager ) {
            return manager->m_categories.at( index );
        }
        return 0;
    }

    static int count_dao(QQmlListProperty<ProductCategory> *list) {
        ProductCategoryManager *manager = qobject_cast<ProductCategoryManager *>(list->object);
        if ( manager ) {
            return manager->m_categories.size();
        }
        return 0;
    }

    static void clear_dao(QQmlListProperty<ProductCategory> *list) {
        ProductCategoryManager *manager = qobject_cast<ProductCategoryManager *>(list->object);
        if ( manager ) {
            manager->m_categories.clear();
            manager->categoriesChanged();
        }
    }

    SqliteDB &m_db;
    std::unique_ptr< AbstractTable<ProductCategory> > m_table;
    QList<ProductCategory* > m_categories;
};

#endif // PRODUCTCATEGORYMANAGER_H
