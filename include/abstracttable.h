#ifndef ABSTRACTTABLE_H
#define ABSTRACTTABLE_H

#include <QMetaObject>
#include <QMetaProperty>
#include <QVector>

#include <QSqlError>
#include <QSqlField>
#include <QSqlQuery>
#include <QSqlRecord>

#include "sqlitedb.h"
#include "sqliteqttypematcher.h"

template<class T> class AbstractTable
{
public:
    AbstractTable(const QString& tblName, SqliteDB& db)
        : m_db(db)
        , m_insertQuery(QSqlQuery(m_db))
        , m_selectQuery(QSqlQuery(m_db))
        , m_deleteQuery(QSqlQuery(m_db))
        , m_updateQuery(QSqlQuery(m_db))
        , m_name(tblName)
    {
        const QMetaObject *t = &T::staticMetaObject;
        for ( int i = t->propertyOffset(); i< t->propertyCount(); ++i )
        {
            QMetaProperty p = t->property(i);
            qDebug() << p.name() << p.typeName() << p.type();
            QSqlField f(p.name(), p.type());
            m_fields.append(f);
            m_rec.append(f);
        }
        initialize();
    }

    QVector<QSqlField> fields() const
    {
        return m_fields;
    }

    bool persist(const T* row)
    {
        const QMetaObject *t = &T::staticMetaObject;
        m_insertQuery.prepare(m_insertStatement);
        for ( int i = t->propertyOffset(); i < t->propertyCount(); ++i ) {
            QMetaProperty p = t->property(i);
            QString pName(":");
            pName.append(p.name());
            m_insertQuery.bindValue(pName, p.read(row));
        }
        //
        if ( ! m_insertQuery.exec() )  {
            QSqlError error = m_insertQuery.lastError();
            qCritical() << Q_FUNC_INFO << "Error code" << error.number();
            qCritical() << "INSERT error description:" << error.text();
            qCritical() << "Continue silently...";
            m_insertQuery.finish();
            return false;
        }
        m_insertQuery.finish();
        m_db.newOperation();
        return true;
    }

    bool selectLike(const QString& field, const QString &likeCondition) {
        QString selectConstraint("SELECT * FROM %1 WHERE %2 LIKE :condition;");
        m_selectQuery.prepare(selectConstraint.arg(m_name).arg(field));
        m_selectQuery.bindValue(":condition", likeCondition);
        bool status = m_selectQuery.exec();
        return status;
    }

    bool selectEqual(const QString& field, const QString &likeCondition) {
        QString selectConstraint("SELECT * FROM %1 WHERE %2 = :condition;");
        m_selectQuery.prepare(selectConstraint.arg(m_name).arg(field));
        m_selectQuery.bindValue(":condition", likeCondition);
        bool status = m_selectQuery.exec();
        return status;
    }

    bool selectSql(const QString &condition = QString())
    {
        if (!condition.isEmpty()) {
            QString selectConstraint("SELECT * FROM %1 WHERE :condition;");
            m_selectQuery.prepare(selectConstraint.arg(m_name));
            m_selectQuery.bindValue(":condition", condition);
            return m_selectQuery.exec();
        }
        else {
            QString selectAll("SELECT * FROM %1;");
            m_selectQuery.prepare(selectAll.arg(m_name));
            return m_selectQuery.exec();
        }
    }

    T* getNextDAO()
    {
        if ( m_selectQuery.next() ) {
            return fromRecord(m_selectQuery.record());
        }
        else {
            m_selectQuery.finish();
            return Q_NULLPTR;
        }
    }

    T* fromRecord( const QSqlRecord& row )
    {
        const QMetaObject *t = &T::staticMetaObject;
        T* item = new T;
        for ( int i = t->propertyOffset(); i< t->propertyCount(); ++i )
        {
            QMetaProperty p = t->property(i);
            p.write(item, row.value(p.name()));
        }
        return item;
    }

    QString name() const
    {
        return m_name;
    }
protected:
    void initialize()
    {
        doInsertStatement();

        if ( ! m_db.tableExists(m_name) ) {
            doCreateTableStatement();
            QSqlQuery creating(m_createTableStatement, m_db);
            if ( ! creating.exec() ) {
                qCritical() << "create table failed";
            }
        }
    }

    void doInsertStatement()
    {
        QTextStream stream(&m_insertStatement);
        QSqlField field;
        stream << "INSERT INTO " << m_name << "(";

        for (int i = 0; i < m_fields.size(); i++) {
            if ( i > 0 ) {
                stream << ", ";
            }
            field = m_fields.at(i);
            stream << field.name();
        }
        stream << ") VALUES(";
        //
        for(int i=0; i<m_fields.size(); i++) {
            if ( i > 0 ) {
                stream << ", ";
            }
            field = m_fields.at(i);
            stream << ":" << field.name();
        }
        stream << ");";
        qDebug() << Q_FUNC_INFO << m_insertStatement;
    }

    void doCreateTableStatement()
    {
        QTextStream stream(&m_createTableStatement);
        QSqlField field;
        stream << "CREATE TABLE IF NOT EXISTS " <<  m_name << " (";
        for ( int i = 0; i < m_fields.size(); ++i ) {
            if ( i > 0 ) {
                stream << ", ";
            }
            field = m_fields.at(i);
            stream << field.name() << " " << m_matcher.getSqliteType(field.type());
        }
        stream << ");";
        qDebug() << Q_FUNC_INFO << m_createTableStatement;
    }

    SqliteDB &m_db;
    QSqlQuery m_insertQuery;
    QSqlQuery m_selectQuery;
    QSqlQuery m_deleteQuery;
    QSqlQuery m_updateQuery;
    QVector<QSqlField> m_fields;
    QString m_name;
    SqliteQtTypeMatcher m_matcher;
    QString m_insertStatement;
    QString m_createTableStatement;
    QSqlRecord m_rec;
};

#endif // ABSTRACTTABLE_H
