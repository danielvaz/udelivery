#ifndef CUSTOMERMANAGER_H
#define CUSTOMERMANAGER_H

#include <memory>

#include <QObject>
#include <QQmlListProperty>

#include "abstracttable.h"
#include "customer.h"

class CustomerManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Customer> customers READ customers NOTIFY customersChanged)
public:
    explicit CustomerManager(SqliteDB& db, QObject *parent = 0);
    virtual ~CustomerManager();

    Q_INVOKABLE bool addCustomer(Customer* c);

    static int declareQml() {
        return qmlRegisterUncreatableType<CustomerManager>("uerp.DB", 1, 0,
                                                           "CustomerManager",
                                                           "CustomerManager is not creatable");
    }

    QQmlListProperty<Customer> customers()
    {
        return QQmlListProperty<Customer>(this, 0, &CustomerManager::append_dao,
                                          &CustomerManager::count_dao,
                                          &CustomerManager::at_dao,
                                          &CustomerManager::clear_dao);
    }

    Q_INVOKABLE void select(const QString& whereClause = QString() );

    Q_INVOKABLE void selectByPhone(const QString& phone);

signals:

    void customersChanged();

public slots:
protected:
    void iterateOverSelect();
private:

    static void append_dao(QQmlListProperty<Customer> *list, Customer *dao) {
        CustomerManager *manager = qobject_cast<CustomerManager *>(list->object);
        if ( manager ) {
            manager->m_customers.append(dao);
            manager->customersChanged();
        }
    }

    static Customer* at_dao(QQmlListProperty<Customer> *list, int index) {
        CustomerManager *manager = qobject_cast<CustomerManager *>(list->object);
        if ( manager ) {
            return manager->m_customers.at( index );
        }
        return 0;
    }

    static int count_dao(QQmlListProperty<Customer> *list) {
        CustomerManager *manager = qobject_cast<CustomerManager *>(list->object);
        if ( manager ) {
            return manager->m_customers.size();
        }
        return 0;
    }

    static void clear_dao(QQmlListProperty<Customer> *list) {
        CustomerManager *manager = qobject_cast<CustomerManager *>(list->object);
        if ( manager ) {
            manager->m_customers.clear();
            manager->customersChanged();
        }
    }

    SqliteDB &m_db;
    std::unique_ptr< AbstractTable<Customer> > m_table;
    QList<Customer* > m_customers;
};

#endif // CUSTOMERMANAGER_H
