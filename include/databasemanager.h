#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QtQml>

#include <QObject>

#include "customermanager.h"
#include "productcategorymanager.h"
#include "productmanager.h"

class DatabaseManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(CustomerManager* customerManager READ customerManager CONSTANT)
    Q_PROPERTY(ProductCategoryManager* productCategoryManager READ productCategoryManager CONSTANT)
    Q_PROPERTY(ProductManager* productManager READ productManager CONSTANT)

public:
    explicit DatabaseManager(QObject *parent = 0);
    ~DatabaseManager() {
        qDebug() << "detroying database manager";
    }

    CustomerManager* customerManager() const
    {
        return m_customerManager;
    }

    ProductCategoryManager* productCategoryManager() const
    {
        return m_productCategoryManager;
    }

    ProductManager* productManager() const
    {
        return m_productManager;
    }

    static int registerSingleton() {
        return qmlRegisterSingletonType<DatabaseManager>("uERP.DB", 1, 0,
                                                         "DatabaseManager",
                                                         &DatabaseManager::newInstance);
    }

    static QObject* newInstance(QQmlEngine *, QJSEngine*) {
        return new DatabaseManager;
    }

private:
    SqliteDB m_db;
    CustomerManager* m_customerManager;
    ProductCategoryManager* m_productCategoryManager;
    ProductManager* m_productManager;
};

#endif // DATABASEMANAGER_H
