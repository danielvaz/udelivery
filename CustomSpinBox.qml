import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

import uDelivery.themes 1.0

Item {
    id: root
    property alias value: spinBox.value
    property alias minimumValue: spinBox.minimumValue
    property alias maximumValue: spinBox.maximumValue

    height: 30
    width: 60

    SpinBox {
        id: spinBox
        anchors.fill: parent

        font.pointSize: Themes.theme.font.size.big

        style: SpinBoxStyle {
            selectionColor: Themes.theme.font.color.textOnDarkBg
            selectedTextColor: Themes.theme.font.color.textOnLightBg
            incrementControl: Image {
                source: "images/Triangular Arrow Up-24.png"
            }
            decrementControl: Image {
                source: "images/Triangular Arrow Down-24.png"
            }
            background: Rectangle {
                anchors.fill: parent
                color: Themes.theme.background.normal
                border.width: 1
            }
        }
    }
}
