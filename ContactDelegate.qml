import QtQuick 2.3

Rectangle {
    readonly property alias newOrderButton: addOrderButton
    id: rect
    width: parent.width
    height: 80

    color: "#ffffff"


    Row {
        id: contactRowLayout
        anchors {
            left: parent.left
        }
        spacing: 10
        Image {
            id: contactIcon
            source: "images/business-contact-32.png"
            verticalAlignment: Image.AlignVCenter
        }
        Text {
            height: contactIcon.height
            text: model.modelData.name
            font.pixelSize: 20
            verticalAlignment: Text.AlignVCenter
        }
    }

    Column {
        anchors {
            left: parent.left
            leftMargin: 50
            top: contactRowLayout.bottom
        }
        Text {

            text: model.modelData.phone
            font.pixelSize: 16
            color: "#555"
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: model.modelData.addressStreet + ", " + model.modelData.addressNumber + ", " + model.modelData.neighborhood
            font.pixelSize: 16
            color: "#555"
            verticalAlignment: Text.AlignVCenter
        }
    }

    PButton {
        id: addOrderButton
        anchors.right: rect.right
        anchors.rightMargin: 30
        anchors.verticalCenter: parent.verticalCenter
        borderWidth: 2
        borderRadius: 26
        width: 50
        height: 50
        borderColor: "#e74c3c"
        iconSource: "images/addorder-32.png"
    }
}
