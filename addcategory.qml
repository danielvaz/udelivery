import QtQuick 2.5
import QtQuick.Controls 1.2
import uERP.DB 1.0

Item {
    id: root

    ProductCategory {
        id: productCategory
        name: nameInput.text
    }

    Rectangle {
        anchors.fill: parent
        color: "#ffffff"
    }

    TopBar {
        id: topBar
        title: "Nova Categoria"
        okButton.onClicked: {
            DatabaseManager.productCategoryManager.addProductCategory(productCategory)
            stackViewPanel.pop()
        }
        cancelButton.onClicked: {
            stackViewPanel.pop()
        }
    }

    ScrollView {

        anchors {
            top: topBar.bottom
            left: root.left
            right: root.right
            bottom: root.bottom
            topMargin: 10
            leftMargin: 30
        }
        contentItem: Grid {
            columns: 2
            spacing: 10

            Text {
                text: "Nome:"
                font.pixelSize: 26
                verticalAlignment: Text.AlignBottom
                height: nameInput.height
            }
            DeliveryTextInput {
                id: nameInput
                verticalAlignment: TextInput.AlignVCenter
            }
        }
    }

}
