import QtQuick 2.3
import QtQuick.Controls 1.2
import uERP.DB 1.0

Item {
    id: root

    Customer {
        id: customer
        name: nameInput.text
        phone: phoneInput.text
        zipCode: zipCodeInput.text
        addressStreet: streetNameInput.text
        addressNumber: parseInt(streetNumberInput.text)
        addressComplement: addressComplementInput.text
        neighborhood: neighborhoodInput.text
    }

    Rectangle {
        anchors.fill: parent
        color: "#ffffff"
    }

    TopBar {
        id: topBar
        title: "Novo Cliente"
        okButton.onClicked: {
            DatabaseManager.customerManager.addCustomer(customer)
            stackViewPanel.pop()
        }
        cancelButton.onClicked: {
            stackViewPanel.pop()
        }
    }

    ScrollView {

        anchors {
            top: topBar.bottom
            left: root.left
            right: root.right
            bottom: root.bottom
            topMargin: 10
            leftMargin: 30
        }
        contentItem: Grid {
            columns: 2
            spacing: 10
            Text {
                text: "Nome:"
                font.pixelSize: 26
                verticalAlignment: Text.AlignBottom
                height: nameInput.height
            }
            DeliveryTextInput {
                id: nameInput
                verticalAlignment: TextInput.AlignVCenter
            }

            Text {
                text: "Telefone:"
                font.pixelSize: 26
                verticalAlignment: Text.AlignBottom
                height: phoneInput.height
            }
            DeliveryTextInput {
                id: phoneInput
                verticalAlignment: TextInput.AlignVCenter
            }

            Text {
                text: "CEP:"
                font.pixelSize: 26
                verticalAlignment: Text.AlignBottom
                height: zipCodeInput.height
            }
            DeliveryTextInput {
                id: zipCodeInput
                verticalAlignment: TextInput.AlignVCenter
            }

            Text {
                text: "Rua:"
                font.pixelSize: 26
                verticalAlignment: Text.AlignBottom
                height: streetNameInput.height
            }
            DeliveryTextInput {
                id: streetNameInput
                verticalAlignment: TextInput.AlignVCenter
            }

            Text {
                text: "Número:"
                font.pixelSize: 26
                verticalAlignment: Text.AlignBottom
                height: streetNumberInput.height
            }
            DeliveryTextInput {
                id: streetNumberInput
                verticalAlignment: TextInput.AlignVCenter
            }

            Text {
                text: "Complemento:"
                font.pixelSize: 26
                verticalAlignment: Text.AlignBottom
                height: streetNumberInput.height
            }
            DeliveryTextInput {
                id: addressComplementInput
                verticalAlignment: TextInput.AlignVCenter
            }

            Text {
                text: "Bairro:"
                font.pixelSize: 26
                verticalAlignment: Text.AlignBottom
                height: neighborhoodInput.height
            }
            DeliveryTextInput {
                id: neighborhoodInput
                verticalAlignment: TextInput.AlignVCenter
            }
        }
    }

}
