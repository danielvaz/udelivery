#include "sqlitedb.h"

SqliteDB::SqliteDB(QSqlDatabase db)
    : QSqlDatabase(db)
    , m_maxOperationsPerTransaction(50)
    , m_currentOperation(0)
{
}

SqliteDB::SqliteDB(QSqlDatabase db, int maxOperationsPerTransaciton)
    : QSqlDatabase(db)
    , m_maxOperationsPerTransaction(maxOperationsPerTransaciton)
    , m_currentOperation(0)
{

}

SqliteDB::~SqliteDB()
{
    doCommit();
    close();
}

bool SqliteDB::openDB()
{
    bool status = open();
    if ( status ) {
        if ( transaction() ) {
            return true;
        } else {
            qCritical() << "open a new transaction failed";
        }
    } else {
        qCritical() << "failed to open the database";
    }
    return false;
}

bool SqliteDB::doTransaction()
{
    if ( commit() ) {
        m_currentOperation = 0;
        if ( transaction() ) {
            return true;
        } else {
            qCritical() << "open a new transaction failed";
        }
    } else {
        qCritical() << "commit the current transaction failed";
    }
    return false;
}

bool SqliteDB::doCommit()
{
    if ( commit() ) {
        m_currentOperation = 0;
        return true;
    }
    qCritical() << "commit the current transaction failed";
    return false;
}

int SqliteDB::newOperation()
{
    m_currentOperation++;
    if ( m_currentOperation >= m_maxOperationsPerTransaction ) {
        if ( ! doTransaction() ) {
            return -1;
        }
    }
    return m_currentOperation;
}

bool SqliteDB::tableExists(const QString &tblName)
{
    QSqlQuery q("SELECT name FROM sqlite_master WHERE type='table'", *this);
    q.exec();
    while ( q.next() )
    {
        if ( q.record().value("name").toString() == tblName )
        {
            q.finish();
            return true;
        }
    }
    return false;
}

QSqlQuery SqliteDB::doExec(const QString& str)
{
    newOperation();
    return exec(str);
}
