#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "customer.h"
#include "product.h"
#include "productmanager.h"
#include "productcategory.h"
#include "productcategorymanager.h"
#include "databasemanager.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    Customer::declareQml();
    ProductCategory::declareQml();
    Product::declareQml();
    CustomerManager::declareQml();
    ProductCategoryManager::declareQml();
    Product::declareQml();
    DatabaseManager::registerSingleton();

    qmlRegisterSingletonType(QUrl(QStringLiteral("qrc:/Themes.qml")),
                             "uDelivery.themes", 1, 0, "Themes");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
