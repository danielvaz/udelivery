#include "databasemanager.h"

DatabaseManager::DatabaseManager(QObject *parent)
    : QObject(parent)
    , m_db(QSqlDatabase::addDatabase( "QSQLITE" ))
{
    m_db.setDatabaseName("uerp.db");
    m_db.openDB();
    //
    m_customerManager = new CustomerManager(m_db, this);
    m_productCategoryManager = new ProductCategoryManager(m_db, this);
    m_productManager = new ProductManager(m_db, this);
}

