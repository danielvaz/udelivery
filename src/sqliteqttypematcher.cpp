#include "sqliteqttypematcher.h"


SqliteQtTypeMatcher::SqliteQtTypeMatcher()
{
    m_typeMatching[QVariant::Double] = QString("REAL");
    m_typeMatching[QVariant::LongLong] = QString("INT8");
    m_typeMatching[QVariant::Int] = QString("INTEGER");
    m_typeMatching[QVariant::String] = QString("TEXT");
    m_typeMatching[QVariant::Bool] = QString("BOOLEAN");
    m_typeMatching[QVariant::ByteArray] = QString("BLOB");
}

QString SqliteQtTypeMatcher::getSqliteType(QVariant::Type qtType) const
{
    if (m_typeMatching.contains(qtType)) {
        return m_typeMatching.value(qtType);
    } else {
        qCritical() << "Critical error in SqliteQtTypeMatcher::getSqliteType(QVariant::Type qtType):"
                    << "Not implemented type matching for" << qtType;
        qFatal("Database will not be created... Aborting application...");
        return QString();
    }
}
