#include "productmanager.h"

ProductManager::ProductManager(SqliteDB &db, QObject *parent)
    : QObject(parent)
    , m_db(db)
    , m_table(new AbstractTable<Product>("ProductTable", m_db))
{
    select();
}

ProductManager::~ProductManager() {
    m_products.clear();
}

bool ProductManager::addProduct(Product *c)
{
    bool status = m_table->persist(c);
    if ( status ) {
        m_products.append(c);
        emit productsChanged();
    }
    return status;
}

void ProductManager::select(const QString &whereClause)
{
    m_table->selectSql(whereClause);
    iterateOverSelect();
}

void ProductManager::selectByCategory(const int category)
{
    m_table->selectEqual("category", QString::number(category));
    iterateOverSelect();
}

void ProductManager::iterateOverSelect()
{
    m_products.clear();
    Product *t = m_table->getNextDAO();
    while ( t != Q_NULLPTR ) {
        t->setParent(this); // avoid memory leaking, so when the parent is destroyed the child shall be destroyed too.
        m_products.append(t);
        t = m_table->getNextDAO();
    }
    emit productsChanged();
}
