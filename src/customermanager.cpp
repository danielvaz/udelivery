#include "customermanager.h"

CustomerManager::CustomerManager(SqliteDB &db, QObject *parent)
    : QObject(parent)
    , m_db(db)
    , m_table(new AbstractTable<Customer>("CustomerTable", m_db))
{

}

CustomerManager::~CustomerManager() {
    qDeleteAll(m_customers);
    m_customers.clear();
}

bool CustomerManager::addCustomer(Customer *c)
{
    bool status = m_table->persist(c);
    if ( status ) {
        m_customers.append(c);
        emit customersChanged();
    }
    return status;
}

void CustomerManager::select(const QString &whereClause)
{
    m_table->selectSql(whereClause);
    iterateOverSelect();
}

void CustomerManager::selectByPhone(const QString &phone)
{
    m_table->selectLike("phone", "%" + phone + "%");
    iterateOverSelect();
}

void CustomerManager::iterateOverSelect()
{
    m_customers.clear();
    Customer *t = m_table->getNextDAO();
    while ( t != Q_NULLPTR ) {
        t->setParent(this); // avoid memory leaking, so when the parent is destroyed the child shall be destroyed too.
        m_customers.append(t);
        t = m_table->getNextDAO();
    }
    emit customersChanged();
}
