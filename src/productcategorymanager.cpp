#include "productcategorymanager.h"

ProductCategoryManager::ProductCategoryManager(SqliteDB &db, QObject *parent)
    : QObject(parent)
    , m_db(db)
    , m_table(new AbstractTable<ProductCategory>("ProductCategoryTable", m_db))
{
    select();
}

ProductCategoryManager::~ProductCategoryManager() {
    m_categories.clear();
}

bool ProductCategoryManager::addProductCategory(ProductCategory *c)
{
    QSqlQuery q = m_db.exec("SELECT MAX(code) FROM " + m_table->name() + ";");
    if ( q.next() ) {
        c->setCode(q.value(0).toInt() + 1);
    } else {
        // primeira categoria a ser inserida
        c->setCode(1);
    }
    bool status = m_table->persist(c);
    if ( status ) {
        m_categories.append(c);
        emit categoriesChanged();
    }
    return status;
}

void ProductCategoryManager::select(const QString &whereClause)
{
    m_table->selectSql(whereClause);
    iterateOverSelect();
}

void ProductCategoryManager::iterateOverSelect()
{
    m_categories.clear();
    ProductCategory *t = m_table->getNextDAO();
    while ( t != Q_NULLPTR ) {
        t->setParent(this); // avoid memory leaking, so when the parent is destroyed the child shall be destroyed too.
        m_categories.append(t);
        t = m_table->getNextDAO();
    }
    emit categoriesChanged();
}
