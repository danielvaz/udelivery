import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.2

ApplicationWindow {
    id: root
    title: qsTr("uERP")
    width: 800
    height: 600
    visible: true

    property var addCustomerPanel: Qt.createComponent("addcustomer.qml")
    property var addProductPanel: Qt.createComponent("addproduct.qml")
    property Component addProductCategoryPanel: Qt.createComponent("addcategory.qml")

    menuBar: MenuBar {
        Menu {
            title: qsTr("&Cadastrar")
            MenuItem {
                text: qsTr("&Produto")
                onTriggered: {
                    stackViewPanel.push(addProductPanel.createObject(root, {'width': root.width, 'height': root.height}))
                }
            }
            MenuItem {
                text: qsTr("Categoria")
                onTriggered: {
                    stackViewPanel.push(addProductCategoryPanel.createObject(root, {'width': root.width, 'height': root.height}))
                }
            }
            MenuItem {
                text: qsTr("&Cliente")
                onTriggered: {

                }
            }
            MenuItem {
                text: qsTr("&Sair")
                onTriggered: Qt.quit();
            }
        }
        Menu {
            title: qsTr("&Pedido")
            MenuItem {
                text: qsTr("&Novo")
                onTriggered: messageDialog.show(qsTr("Novo pedido"));
            }
            MenuItem {
                text: qsTr("&Buscar")
                onTriggered: messageDialog.show(qsTr("Buscar pedido"));
            }
        }
    }

    StackView {
        id: stackViewPanel
        anchors.fill: parent
        initialItem: MainScreen {
            width: root.width
            height: root.height
        }
    }
}
