TEMPLATE = app

QT += qml quick sql

CONFIG += c++11

TARGET = dist/uerp
OBJECTS_DIR = obj
MOC_DIR = obj/moc

INCLUDEPATH += include

DEPENDPATH += src include

SOURCES += src/main.cpp \
    src/sqlitedb.cpp \
    src/sqliteqttypematcher.cpp \
    src/customermanager.cpp \
    src/databasemanager.cpp \
    src/productmanager.cpp \
    src/productcategorymanager.cpp

HEADERS += \
    include/customer.h \
    include/abstracttable.h \
    include/sqlitedb.h \
    include/sqliteqttypematcher.h \
    include/customermanager.h \
    include/databasemanager.h \
    include/product.h \
    include/productmanager.h \
    include/productcategory.h \
    include/productcategorymanager.h

RESOURCES += qml.qrc \
    images.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
