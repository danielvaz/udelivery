import QtQuick 2.3
import QtQuick.Controls 1.2

Rectangle {
    id: root
    color: "#F0F0F0"

    property var addCustomerPanel: Qt.createComponent("addcustomer.qml")
    property var searchCustomerPanel: Qt.createComponent("searchcustomerpanel.qml")
    property var newOrderPanel: Qt.createComponent("neworderpanel.qml")

    Row {
        spacing: 50
        anchors.horizontalCenter: root.horizontalCenter
        anchors.verticalCenter: root.verticalCenter
        PButton {
            borderWidth: 2
            borderRadius: 10
            borderColor: "#CCCCCC"
            iconSource: "images/adduser-64.png"
            colorReleased: "#FFFFFF"
            colorPressed: "#AAFFFFFF"
            onClicked: {
                stackViewPanel.push(addCustomerPanel.createObject(root, {'width': root.width, 'height': root.height}))
            }
        }

        PButton {
            borderWidth: 2
            borderRadius: 10
            borderColor: "#CCCCCC"
            iconSource: "images/search-64.png"
            colorReleased: "#FFFFFF"
            colorPressed: "#AAFFFFFF"
            onClicked: {
                stackViewPanel.push(searchCustomerPanel.createObject(root, {'width': root.width, 'height': root.height}))
            }
        }

        PButton {
            borderWidth: 2
            borderRadius: 10
            borderColor: "#CCCCCC"
            iconSource: "images/shoppingcart-64.png"
            colorReleased: "#FFFFFF"
            colorPressed: "#AAFFFFFF"
            onClicked: {
                stackViewPanel.push(newOrderPanel.createObject(root, {'width': root.width, 'height': root.height}))
            }
        }
    }

}
